import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { GenerateLinkService } from '../../service/generate-link.service'

@Component({
  selector: 'app-redirect-link',
  templateUrl: './redirect-link.component.html',
  styleUrls: ['./redirect-link.component.css']
})
export class RedirectLinkComponent implements OnInit {

  constructor(
    private activatedRote: ActivatedRoute,
    private generateLinkService: GenerateLinkService,
  ) { }

  ngOnInit(): void {
    this.activatedRote.params.subscribe(params => {
      this.generateLinkService.GetMainLink(params['shortLink']).subscribe((data)=>{
        let preHttp = ''
        if(!data.mainLink.includes('https') && !data.mainLink.includes('http')){
          preHttp = 'https://'
        }
        window.location.href = preHttp + data.mainLink
      })
    })
  }

}
