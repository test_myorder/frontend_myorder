import { Component, NgZone, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { GenerateLinkService } from '../../service/generate-link.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-generate-link',
  templateUrl: './generate-link.component.html',
  styleUrls: ['./generate-link.component.css']
})
export class GenerateLinkComponent implements OnInit {
  inputForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private generateLinkService: GenerateLinkService,
    private ngZone: NgZone,
    private router: Router,
  ) {
    this.inputForm = this.formBuilder.group({
      mainLink: [''],
    })
  }

  ngOnInit(): void {
  }

  onSubmit():any {
    this.generateLinkService.Generate(this.inputForm.value)
      .subscribe((data) => {
        window.alert('Your short link is: ' + data.shortLink)
        this.ngZone.run(() => this.router.navigateByUrl('/'))
      }, (error) => {
        console.log(error)
      })
  }

}
