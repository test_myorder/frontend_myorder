import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { catchError, map, Observable, throwError } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class GenerateLinkService {

  HOST_API: string = 'http://localhost:3500'

  constructor(private httpClient: HttpClient) {
  }

  Generate(link: object): Observable<any> {
    let API_URL = `${this.HOST_API}/generate-link`
    return this.httpClient.post(API_URL, link).pipe(
      map((res: any) => {
        return res || {}
      }),
      catchError((err) => {
        return throwError(err)
      }),
    )
  }

  GetMainLink(shortLink: string): Observable<any> {
    let API_URL = `${this.HOST_API}/generate-link/${shortLink}`
    return this.httpClient.get(API_URL).pipe(
      map((res: any) => {
        return res
      }),
    )
  }
}
