import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RedirectLinkComponent } from './components/redirect-link/redirect-link.component';
import { AppRoutingModule } from './app-routing.module';
import { GenerateLinkComponent } from './components/generate-link/generate-link.component'

@NgModule({
  declarations: [
    AppComponent,
    RedirectLinkComponent,
    GenerateLinkComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
