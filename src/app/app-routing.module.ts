import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { RedirectLinkComponent } from './components/redirect-link/redirect-link.component'
import { GenerateLinkComponent } from './components/generate-link/generate-link.component'

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'generateLink'},
  {path: 'generateLink', component: GenerateLinkComponent},
  {path: ':shortLink', component: RedirectLinkComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
